#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include <QClipboard>
#include "ColorDialog.h"

class Controller : public QObject
{
	QClipboard *cb;
	ColorDialog *cd;
	
private slots:
	void onColorChange(const QColor&);
	void onWindowChanged();
	
public:
	Controller(QObject *parent = 0);
	~Controller();
};


#endif //CONTROLLER_H