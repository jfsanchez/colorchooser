#include "ColorDialog.h"

ColorDialog::ColorDialog(QWidget* parent):
QColorDialog(QColor(57,124,119),parent)
{
}


void ColorDialog::changeEvent(QEvent *event)
{
	emit windowChanged();
    QColorDialog::changeEvent(event);
}
