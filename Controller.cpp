#include "Controller.h"
#include <QApplication>
#include <QDebug>


Controller::Controller(QObject* parent):
QObject(parent)
{
	cb = QApplication::clipboard();
	cd = new ColorDialog();
	connect(cd, &ColorDialog::currentColorChanged, this, &Controller::onColorChange);
	connect(cd, &ColorDialog::windowChanged, this, &Controller::onWindowChanged);
	cd->show();
}

Controller::~Controller()
{
}

void Controller::onColorChange(const QColor &color)
{
	QString hexColor = color.name(QColor::HexRgb);
	cb->setText(hexColor);
}

void Controller::onWindowChanged()
{
	QString hexColor1 = cb->text();
	QString hexColor2 = cd->currentColor().name(QColor::HexRgb);
// 	qDebug() << QString("color1: %1; color2: %2").arg(hexColor1).arg(hexColor2);
	
	if(hexColor1 != hexColor2) {
		QColor color(hexColor1);
		cd->setCurrentColor(color);
	}
}
