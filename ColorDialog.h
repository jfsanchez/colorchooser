#ifndef COLORDIALOG_H
#define COLORDIALOG_H

#include <QColorDialog>
#include <QEvent>


class ColorDialog : public QColorDialog
{
	Q_OBJECT
private:
		
	virtual void changeEvent(QEvent *event);

public:
	ColorDialog(QWidget *parent = 0);
	
signals:
	void windowChanged();
};



#endif //COLORDIALOG_H