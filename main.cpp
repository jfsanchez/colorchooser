/*
 * File:   main.cpp
 * Author: jfsanchez
 *
 * Created on June 30, 2014, 3:05 PM
 */

#include <QApplication>
#include "Controller.h"

int main(int argc, char *argv[]) {
    // initialize resources, if needed
    // Q_INIT_RESOURCE(resfile);

    QApplication app(argc, argv);
	
	Controller con;

    return app.exec();
}
